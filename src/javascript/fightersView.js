import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import  fighterDetailView from './fighterDetailView';

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

    async handleFighterClick(event, fighter) {
        this.handleOkClick = this.okClick.bind(this);
        this.handleChange = this.change.bind(this);
          if (this.fightersDetailsMap.get(fighter._id) === undefined){
              const fighterDetails = await fighterService.getFighterDetails(fighter._id);
              this.fightersDetailsMap.set(fighter._id, fighterDetails);
            }

        const fighterDetailsElement = await new fighterDetailView(
                      this.fightersDetailsMap.get(fighter._id),
                      this.handleOkClick,
                      this.handleChange
          );
          let info=document.getElementById('info');
          while (info.firstChild) {
              info.removeChild(info.firstChild);
          }
          info.appendChild(fighterDetailsElement.element);
          info.style.display = 'flex';

    // get from map or load info and add to fightersMap
    // show modal with fighter info
    // allow to edit health and power in this modal

  }

    okClick(event){
        document.getElementById('info').style.display = 'none';
    }

    change (event, _id, key) {
       this.fightersDetailsMap.get(_id)[key] = event.srcElement.value;
        console.log(this.fightersDetailsMap.get(_id));
    }
}

export default FightersView;