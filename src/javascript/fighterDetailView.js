import View from './view';

class FighterDetailView extends View {
    constructor(details, okClick, change) {
        super();

        this.createDetailWindow(details, okClick, change);
    }

    createDetailWindow(details, okClick, change) {
        const {_id, name, health, attack, defense} = details;
        const nameElement = this.createName(name);
        const healthElement = this.createHealth(_id, health, change);
        const attackElement = this.createAttack(_id, attack, change);
        const defenseElement = this.createDefense(_id, defense, change);
        const okButton = this.createOk(okClick);

       this.element = this.createElement({ tagName: 'div', className: 'detail' });
       this.element.append(nameElement, healthElement, attackElement, defenseElement, okButton);
       //this.element.addEventListener('click', event => okClick(event), false);
    }

    createName(name) {
        const nameElement = this.createElement({ tagName: 'span', className: 'name' });
        nameElement.innerText = name;

        return nameElement;
    }

    createHealth(_id, health, change) {
        const healthElement = this.createElement({tagName: 'input', className: 'health'});
        healthElement.value = health;
        healthElement.addEventListener('change', event => change(event, _id, 'health'), false);

        return healthElement;
    }

    createAttack(_id, attack, change) {
        const attackElement = this.createElement({tagName: 'input', className: 'attack'});
        attackElement.value = attack;
        attackElement.addEventListener('change', event => change(event, _id, 'attack'), false);

        return attackElement;
    }

    createDefense(_id, defense, change) {
        const defenseElement = this.createElement({tagName: 'input', className: 'defense'});
        defenseElement.value = defense;
        defenseElement.addEventListener('change', event => change(event, _id, 'defense'), false);

        return defenseElement;
    }

    createOk(okClick) {
        const okElement = this.createElement({tagName: 'input', className: 'ok'});
        okElement.type = 'button';
        okElement.value = 'Ok';
        okElement.addEventListener('click', event => okClick(event), false);

        return okElement;
    }

}

export default FighterDetailView;