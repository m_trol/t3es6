import { fighterService } from './services/fightersService';
import FighterView from './fighterView';
import FightersView from  './fightersView';

class Fighter {
    constructor(fighter) {

    }

    random(min, max) {
        let rand = min - 0.5 + Math.random() * (max - min + 1);
        rand = Math.round(rand);

        return rand;
    }

    getHitPower() {
        let criticalHitChance = this.random(1, 2);

        return attack * criticalHitChance;
    }

    getBlockPower() {
        let dodgeChance = this.random(1, 2);

        return defense * dodgeChance;
    }


}

export default Fighter;